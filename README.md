# CF7 material like form for wordpress
## dependencies foundation-grid-XY, jQuery
- foundation only in snippet couple lines under this. (easy to change for bootstrap)
- jQuery needed for reorder native ContactForm7 label placement
## How to deploy
1. include `form.js` to script load queue (static html or sage)
2. include `forms.scss` to main.scss
3. Use this CF7 snippet
```HTML
<div class="grid-x grid-padding-x">
  <div class="cell large-6">
    <label class="for-input"> Name </label>
     [text* your-name]
    <label class="for-input"> E-mail </label>
     [email* your-email] 
    <label class="for-input"> Phone </label>
     [tel* your-phone]
  </div>
  <div class="cell large-6">
   <label class="for-input">Message</label>
    [textarea your-message] 
  </div>
</div>
<div class="line">
  [acceptance accept-pd] Check here if you accept <a href="terms">General Data Protection terms</a>. [/acceptance]
</div>
<div class="line">
  [submit class:btn-secondary "Send words"]
</div>
```

Contributors: @pepiess, @robertJurinek (thanks for cross-browser)
