jQuery(document).ready(() => {
    // Place label as sibling to input (so we can use ~ selector)
    // Dafult CF7 is: <label><span><input></span >
    $("#contact-form label.for-input").each(function(){
        $(this).appendTo($(this).next());
    });

    $('#contact-form div.cell input, #contact-form div.cell textarea').blur(function(){
        var tmpval = $(this).val();
        if(tmpval == '') {
            $(this).addClass('empty');
            $(this).removeClass('not-empty');
        } else {
            $(this).addClass('not-empty');
            $(this).removeClass('empty');
        }
    });
});
  